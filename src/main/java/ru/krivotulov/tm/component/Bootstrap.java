package ru.krivotulov.tm.component;

import ru.krivotulov.tm.api.controller.ICommandController;
import ru.krivotulov.tm.api.controller.IProjectController;
import ru.krivotulov.tm.api.controller.IProjectTaskController;
import ru.krivotulov.tm.api.controller.ITaskController;
import ru.krivotulov.tm.api.repository.ICommandRepository;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.ICommandService;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.constant.ArgumentConst;
import ru.krivotulov.tm.constant.TerminalConst;
import ru.krivotulov.tm.controller.CommandController;
import ru.krivotulov.tm.controller.ProjectController;
import ru.krivotulov.tm.controller.ProjectTaskController;
import ru.krivotulov.tm.controller.TaskController;
import ru.krivotulov.tm.repository.CommandRepository;
import ru.krivotulov.tm.repository.ProjectRepository;
import ru.krivotulov.tm.repository.TaskRepository;
import ru.krivotulov.tm.service.CommandService;
import ru.krivotulov.tm.service.ProjectService;
import ru.krivotulov.tm.service.ProjectTaskService;
import ru.krivotulov.tm.service.TaskService;
import ru.krivotulov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        commandController.displayWelcome();
        process();
    }

    private void process() {
        while (true) {
            System.out.println("ENTER COMMAND:");
            runCommand(TerminalUtil.readLine());
        }
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case (TerminalConst.COMMANDS):
                commandController.displayCommands();
                break;
            case (TerminalConst.ARGUMENTS):
                commandController.displayArguments();
                break;
            case (TerminalConst.HELP):
                commandController.displayHelp();
                break;
            case (TerminalConst.VERSION):
                commandController.displayVersion();
                break;
            case (TerminalConst.ABOUT):
                commandController.displayAbout();
                break;
            case (TerminalConst.INFO):
                commandController.displaySystemInfo();
                break;
            case (TerminalConst.TASK_LIST):
                taskController.displayTaskList();
                break;
            case (TerminalConst.TASK_CLEAR):
                taskController.clearTasks();
                break;
            case (TerminalConst.TASK_CREATE):
                taskController.createTask();
                break;
            case (TerminalConst.TASK_SHOW_BY_ID):
                taskController.showTaskById();
                break;
            case (TerminalConst.TASK_SHOW_BY_INDEX):
                taskController.showTaskByIndex();
                break;
            case (TerminalConst.TASK_UPDATE_BY_ID):
                taskController.updateTaskById();
                break;
            case (TerminalConst.TASK_UPDATE_BY_INDEX):
                taskController.updateTaskByIndex();
                break;
            case (TerminalConst.TASK_REMOVE_BY_ID):
                taskController.removeTaskById();
                break;
            case (TerminalConst.TASK_REMOVE_BY_INDEX):
                taskController.removeTaskByIndex();
                break;
            case (TerminalConst.TASK_CHANGE_STATUS_BY_ID):
                taskController.changeTaskStatusById();
                break;
            case (TerminalConst.TASK_CHANGE_STATUS_BY_INDEX):
                taskController.changeTaskStatusByIndex();
                break;
            case (TerminalConst.TASK_START_BY_ID):
                taskController.startTaskById();
                break;
            case (TerminalConst.TASK_START_BY_INDEX):
                taskController.startTaskByIndex();
                break;
            case (TerminalConst.TASK_COMPLETE_BY_ID):
                taskController.completeTaskById();
                break;
            case (TerminalConst.TASK_COMPLETE_BY_INDEX):
                taskController.completeTaskByIndex();
                break;
            case (TerminalConst.TASK_SHOW_LIST_BY_PROJECT):
                taskController.displayTaskListByProject();
                break;
            case (TerminalConst.PROJECT_LIST):
                projectController.displayProjectList();
                break;
            case (TerminalConst.PROJECT_CLEAR):
                projectController.clearProjects();
                break;
            case (TerminalConst.PROJECT_CREATE):
                projectController.createProject();
                break;
            case (TerminalConst.PROJECT_SHOW_BY_ID):
                projectController.showProjectById();
                break;
            case (TerminalConst.PROJECT_SHOW_BY_INDEX):
                projectController.showProjectByIndex();
                break;
            case (TerminalConst.PROJECT_UPDATE_BY_ID):
                projectController.updateProjectById();
                break;
            case (TerminalConst.PROJECT_UPDATE_BY_INDEX):
                projectController.updateProjectByIndex();
                break;
            case (TerminalConst.PROJECT_REMOVE_BY_ID):
                projectController.removeProjectById();
                break;
            case (TerminalConst.PROJECT_REMOVE_BY_INDEX):
                projectController.removeProjectByIndex();
                break;
            case (TerminalConst.PROJECT_CHANGE_STATUS_BY_ID):
                projectController.changeProjectStatusById();
                break;
            case (TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX):
                projectController.changeProjectStatusByIndex();
                break;
            case (TerminalConst.PROJECT_START_BY_ID):
                projectController.startProjectById();
                break;
            case (TerminalConst.PROJECT_START_BY_INDEX):
                projectController.startProjectByIndex();
                break;
            case (TerminalConst.PROJECT_COMPLETE_BY_ID):
                projectController.completeProjectById();
                break;
            case (TerminalConst.PROJECT_COMPLETE_BY_INDEX):
                projectController.completeProjectByIndex();
                break;
            case (TerminalConst.BIND_TASK_TO_PROJECT):
                projectTaskController.bindTaskToProject();
                break;
            case (TerminalConst.UNBIND_TASK_FROM_PROJECT):
                projectTaskController.unbindTaskFromProject();
                break;
            case (TerminalConst.EXIT):
                close();
                break;
            default:
                commandController.displayError(command);
                break;
        }
    }

    private boolean runArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String param = args[0];
        runArgument(param);
        return true;
    }

    private void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case (ArgumentConst.COMMANDS):
                commandController.displayCommands();
                break;
            case (ArgumentConst.ARGUMENTS):
                commandController.displayArguments();
                break;
            case (ArgumentConst.HELP):
                commandController.displayHelp();
                break;
            case (ArgumentConst.VERSION):
                commandController.displayVersion();
                break;
            case (ArgumentConst.ABOUT):
                commandController.displayAbout();
                break;
            case (ArgumentConst.INFO):
                commandController.displaySystemInfo();
                break;
            default:
                commandController.displayError(arg);
                break;
        }
    }

    private static void close() {
        System.exit(0);
    }

}
