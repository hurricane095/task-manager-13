package ru.krivotulov.tm.api.controller;

public interface IProjectController {

    void displayProjectList();

    void clearProjects();

    void createProject();

    void removeProjectByIndex();

    void removeProjectById();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

}
