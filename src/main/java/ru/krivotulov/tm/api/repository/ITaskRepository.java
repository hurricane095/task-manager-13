package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task delete(Task task);

    Task deleteById(String id);

    Task deleteByIndex(Integer index);

    void clear();

}
