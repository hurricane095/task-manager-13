package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
