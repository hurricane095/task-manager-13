package ru.krivotulov.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public interface TerminalUtil {

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    static String readLine() {
        try {
            return bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    static Integer nextNumber() {
        try {
            String value = bufferedReader.readLine();
            return Integer.parseInt(value);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
