package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projectList = new ArrayList<>();

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projectList.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectList;
    }

    @Override
    public Project delete(final Project project) {
        projectList.remove(project);
        return project;
    }

    @Override
    public Project findOneById(String id) {
        for (final Project project : projectList) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index > projectList.size() || projectList.isEmpty()) return null;
        return projectList.get(index);
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public Project deleteById(String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        projectList.remove(project);
        return project;
    }

    @Override
    public Project deleteByIndex(Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        projectList.remove(project);
        return project;
    }

    @Override
    public void clear() {
        projectList.clear();
    }

}
